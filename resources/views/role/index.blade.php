@extends('layouts.app')

@section('content')
    <h1>Lista de roles</h1>

    <ul>
    @forelse ($roles as $role)
        <li>{{ $role->name }}
            @can('view',$role)
            <a href="/roles/{{ $role->id }}">Ver</a>
            <form method="post" action="/roles/{{ $role->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
        @can ('delete',$role)
                <input type="submit" value="borrar">
        @endcan
            </form>
            @endcan
        </li>
    @empty
        <li>No hay roles!!</li>
    @endforelse
    </ul>

    {{ $roles->render() }}

@endsection
