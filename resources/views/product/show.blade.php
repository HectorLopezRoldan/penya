@extends('layouts.app')

@section('title', 'Productos')

@section('content')

    <h1>
        Este es el detalle del producto <?php echo $product->id ?>
    </h1>

    <ul>
        <li>Nombre: {{ $product->name }}</li>
        <li>Precio: {{ $product->price }}</li>
        <li>Categoria: {{  $product->cathegory->name}}</li>
    </ul>

@endsection

