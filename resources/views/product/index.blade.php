@extends('layouts.app')

@section('content')
    <h1>Lista de productos</h1>
    <a href="/products/create">Nuevo</a>
    <ul>
    @forelse ($products as $product)
        <li>Nombre->{{ $product->name }}<br>

         Categoría-> {{$product->cathegory->name}}<br>
         Precio-> {{$product->price}}
            <a href="/products/{{ $product->id }}/edit">Editar</a>
            <a href="/products/{{ $product->id }}">Ver</a>
            <a class="btn btn-success" href="/basket/{{ $product->id }}">Añadir a la cesta</a>
            <form method="post" action="/products/{{ $product->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="borrar">
            </form>
        </li>
    @empty
        <li>No hay productos!!</li>
    @endforelse
    </ul>

    {{ $products->render() }}

@endsection
