@extends('layouts.app')

@section('title', 'Productos')

@section('content')
    <h1>Editar producto</h1>

    <form method="post" action="/products/{{ $product->id }}">
        {{ csrf_field() }}

        <input type="hidden" name="_method" value="PUT">

        <label>Nombre</label>
        <input type="text" name="name"
        value="{{ old('name') ? old('name') : $product->name }}">
        <div class="alert alert-danger">
            {{ $errors->first('name') }}
        </div>
        <br>

        <br>

        <label>Precio</label>
        <input type="text" name="price"
        value="{{ old('price') ? old('price') : $product->price }}">
        <div class="alert alert-danger">
            {{ $errors->first('price') }}
        </div>
        <br>

        <?php
            use App\Cathegory;
            $cathegories = Cathegory::all();
        ?>

        <label>Categoría</label>
        <select name="cathegory_id">
        @foreach ($cathegories as $cathegory)
            <option value="{{ $cathegory->id }}"
            {{ old('cathegory_id') == $cathegory->id ?
            'selected="selected"' :
            ''
            }}>{{ $cathegory->name }}
        </option>
        @endforeach
        </select>

        <br>

        <input type="submit" value="Guardar Cambios">
    </form>
@endsection
