@extends('layouts.app')

@section('title', 'Productos')

@section('content')
<style type="text/css">
    .alert {
      padding: 5px;
      background-color: #faa; /* Red */
      margin: 5px;
    }
</style>
    <h1>Alta de productos</h1>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{--
    --}}

    @if ($errors->any())
        <div class="alert alert-danger">
            Se han producido errores de validación
        </div>
    @endif

    <form method="post" action="/products">
        {{ csrf_field() }}

        <label>Nombre</label>
        <input type="text" name="name" value="{{ old('name') }}">
        <div class="alert alert-danger">
            {{ $errors->first('name') }}
        </div>
        <br>

        <label>Precio</label>
        <input type="text" name="price" value="{{ old('price') }}">
        <div class="alert alert-danger">
            {{ $errors->first('price') }}
        </div>

        <br>

        <?php
            $example = ['rojo', 'azul', 'verde'];
            $html = '<hr>';
        ?>
        <select name="color">
        @foreach ($example as $item)
            <option value="{{ $item }}"
            {{ old('color') == $item ?
            'selected="selected"' :
            ''
            }}>{{ $item }}
        </option>
        @endforeach
        </select>
        <br>

       <?php
            use App\Cathegory;
            $cathegories = Cathegory::all();
        ?>
        <label>Categoría</label>
        <select name="cathegory_id">
        @foreach ($cathegories as $cathegory)
            <option value="{{ $cathegory->id }}"
            {{ old('cathegory_id') == $cathegory->id ?
            'selected="selected"' :
            ''
            }}>{{ $cathegory->name }}
        </option>
        @endforeach
        </select>







     <!--   <input type="category_id" name="category_id">-->

        <br>

        <input type="submit" value="Nuevo">
    </form>
@endsection
