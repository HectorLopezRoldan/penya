@extends('layouts.app')

@section('content')
    <h1>Productos en la cesta</h1>
    <ul>
    @forelse ($products as $product)
        <li>{{ $product->id }}: {{ $product->name }} cantidad -> {{ $product->cantidad }}
            <a href="/products/{{ $product->id }}/edit">Editar</a>
            <a href="/products/{{ $product->id }}">Ver</a>
            <a href="/basket/removeProduct/{{ $product->id }}" class="btn btn-primary">
                -
            </a>
            <a href="/basket/destroy/{{ $product->id }}" class="btn btn-primary">
                Quitar
            </a>
            <a href="/basket/addProductBasket/{{ $product->id }}" class="btn btn-primary">
                +
            </a>
        </li>
    @empty
        <li>No hay na!!</li>
    @endforelse
    </ul>

     @if(Session::get('basket') != null)
        <div class="text-center">
            <a href="/basket/confirm" class="btn btn-success">
              Confirmar pedido
            </a>
        </div>
@endif
@endsection
