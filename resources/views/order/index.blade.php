

@extends('layouts.app')

@section('content')
    <h1>Pedidos</h1>
    <hr>
    <table width="100%">
        <tr><th>Fecha</th><th>Precio total</th><th>Usuario</th><th>Pagado</th><th></th></tr>
        @forelse ($orders as $order)
        <tr>
            <td>{{ date("d/m/Y", strtotime($order->date)) }}</td>
            <td>{{$order->total() }}€</td>
            <td>{{ $order->user->name }}</td>
            <td>@if($order->paid == 0) No @else Si @endif</td>
            <td><a href="/orders/{{ $order->id }}">Ver</a>
        </td>
        </tr>
    @empty
        <td>No hay pedidos!!</td>
    @endforelse
    </table>

    {{ $orders->render() }}
    <a class="btn btn-success" href="/order/toPDF">Exportar a PDF</a>
@endsection
