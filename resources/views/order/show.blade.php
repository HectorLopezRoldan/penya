@extends('layouts.app')

@section('title', 'Categoria')

@section('content')

<h1>
    Este es el detalle del pedido <?php echo $order->id ?>
</h1>

<ul>
    <li>Fecha: {{ date("d/m/Y", strtotime($order->date)) }}</li>
    <li>Total: {{ $order->total() }}€</li>
    <li>Pagado:
        @if($order->paid == 0) No
            @can('viewAllOrders',$order)<a href="/orders/paid/{{ $order->id }}" class="btn btn-primary">Pagar</a>@endcan
        @else Si
        @endif</li>
    <li>Usuario: {{ $order->user->name }}</li>
</ul>

<h2>
    Productos del pedido:
</h2>
<ul>
    @foreach($products as $product)
    <li>{{$product->name}} - {{$product->price}}€ - {{$product->pivot->quantity}} unidades</li>
    @endforeach
</ul>

 <a class="btn btn-success" href="/order/toPDFshow{{$order->id}}">Exportar a PDF</a>

@endsection
