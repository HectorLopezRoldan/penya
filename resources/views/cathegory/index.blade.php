@extends('layouts.app')

@section('content')
    <h1>Lista de categorias</h1>
    <a href="/cathegories/create">Nuevo</a>
    <ul>
    @forelse ($cathegories as $category)
        <li>{{ $category->id }}: {{ $category->name }}
            <a href="/cathegories/{{ $category->id }}/edit">Editar</a>
            <a href="/cathegories/{{ $category->id }}">Ver</a>

            <!--<form method="post" action="/cathegories/{{ $category->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">

                <input type="submit" value="borrar">
            </form>-->
        </li>
    @empty
        <li>No hay categorias!!</li>
    @endforelse
    </ul>

    {{ $cathegories->render() }}

@endsection
