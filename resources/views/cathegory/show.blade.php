@extends('layouts.app')

@section('title', 'Categorias')

@section('content')

    <h1>
        Este es el detalle de la categoria <?php echo $cathegory->id ?>
    </h1>

    <ul>
        <li>Nombre: {{ $cathegory->name }}</li>
    </ul><br>
    <h1>Productos asociados</h1>

    @forelse ($products as $product)
        <li>Nombre->{{ $product->name }}. Categoría-> {{$product->cathegory->name}}
            <a href="/products/{{ $product->id }}/edit">Editar</a>
            <a href="/products/{{ $product->id }}">Ver</a>
            <form method="post" action="/products/{{ $product->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="borrar">
            </form>
        </li>
    @empty
        <li>No hay productos!!</li>
    @endforelse


@endsection

