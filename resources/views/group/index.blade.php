@extends('layouts.app')

@section('content')
    <h1>Grupo de usuarios</h1>
    <a href="/users/create">Nuevo</a>
    <ul>
    @forelse ($users as $user)
        <li>{{ $user->name }}: {{ $user->email }} : {{ $user->role->name }} : {{ $user->cantidad }}
             @can ('update',$user)
            <a href="/users/{{ $user->id }}/edit">Editar</a>
             @endcan
            <a href="/users/{{ $user->id }}">Ver</a>
            <form method="post" action="/users/{{ $user->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
        @can ('delete',$user)
                <input type="submit" value="borrar">
        @endcan
        </form>

        </li>
    @empty
        <li>No hay usuarios!!</li>
    @endforelse
    </ul>

 <a href="/groups/flush">Vaciar lista</a>

@endsection
