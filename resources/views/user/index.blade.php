@extends('layouts.app')

@section('content')
    <h1>Lista de usuarios</h1>
    <a href="/users/create">Nuevo</a>
    <ul>
    @forelse ($users as $user)
        <li>{{ $user->name }}: {{ $user->email }} : {{ $user->role->name }}
             @can ('update',$user)
            <a href="/users/{{ $user->id }}/edit">Editar</a>
             @endcan
            <a href="/users/{{ $user->id }}">Ver</a>
            <form method="post" action="/users/{{ $user->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
        @can ('delete',$user)
                <input type="submit" value="borrar">
        @endcan
        </form>
        <a class="btn btn-success" href="/groups/{{ $user->id }}">Guardar</a>

        </li>
    @empty
        <li>No hay usuarios!!</li>
    @endforelse
    </ul>

    {{ $users->render() }}

@endsection
