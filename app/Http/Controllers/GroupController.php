<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;
class GroupController extends Controller
{

    public function index(Request $request)
    {

        $group=$request->session()->get('group');


        if($group==null){
            $group=array();
        }

     return view('group.index',['users'=> $group]);
    }
    public function addUser(Request $request,$id)
    {
        $user=User::findOrFail($id);
        $group=$request->session()->get('group');
        if($group==null){
            $group=array();
        }
        $position=-1;
        foreach ($group as $key=>$item) {
            if ($item->id == $user->id) {
                $item->cantidad++;
                $position=$key;
                break;
            }
        }

        if($position==-1){
            $user->cantidad=1;
            $request->session()->push('group', $user);
        }

        return redirect('/groups');
    }

    public function flush(Request $request)
    {
        $request->session()->forget('group');
        return redirect('/groups');
    }
}
