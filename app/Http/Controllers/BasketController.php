<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;
use App\Product;
use App\Order;
use NahidulHasan\Html2pdf\Facades\Pdf;
class BasketController extends Controller
{

    public function __construct()
    {
        // $this->middleware('guest');
        //si estas logeado puedes verlo
        $this->middleware('auth');
    }


    public function index(Request $request)
    {

        $basket=$request->session()->get('basket');
        if($basket==null){
            $basket=array();
        }
     return view('basket.index',['products'=> $basket]);
    }

    public function addProduct(Request $request,$id)
    {
        $product=Product::findOrFail($id);
        $basket=$request->session()->get('basket');
        if($basket==null){
            $basket=array();
        }
        $position=-1;
        foreach ($basket as $key=>$item) {
            if ($item->id == $product->id) {
                $item->cantidad++;
                $position=$key;
                break;
            }
        }
        if($position==-1){
            $product->cantidad=1;
             $request->session()->push('basket', $product);
        }
        return redirect('/products');
    }

    public function addProductBasket(Request $request,$id)
    {
        $product=Product::findOrFail($id);
        $basket=$request->session()->get('basket');
        if($basket==null){
            $basket=array();
        }
        $position=-1;
        foreach ($basket as $key=>$item) {
            if ($item->id == $product->id) {
                $item->cantidad++;
                $position=$key;
                break;
            }
        }
        if($position==-1){
            $product->cantidad=1;
             $request->session()->push('basket', $product);
        }
        return redirect('/basket');
    }


    public function destroy(Request $request,$id)
    {
        $product = Product::findOrFail($id);

        $basket = $request->session()->get('basket');



        $position = -1;
        foreach ($basket as $key => $product) {
            if($product->id == $id){
                $position = $key;
                break;
            }
        }

        if($position != -1){
            Session::forget('basket.'.$position);
        }
        return redirect('/basket');
    }

    public function removeProduct(Request $request,$id)
    {
        $product = Product::findOrFail($id);

        $basket = $request->session()->get('basket');

        $position = -1;
        foreach ($basket as $key => $product) {
            if($product->id == $id){
                $position = $key;
                break;
            }
        }

        if($position != -1){
            $basket[$position]->cantidad--;
            if($basket[$position]->cantidad <= 0){
                Session::forget('basket.'.$position);
            }
        }
         return redirect('/basket');
    }

    //Para insertar en la tabla intermedia crearemos el objeto order primero para luego en base al mismo
    //poder sacar sus productos y con el metodo attach indicaremos los campos a introducir
    public function confirm(Request $request)
    {
        if($request->session()->get('basket') != null){
                $basket = $request->session()->get('basket');
                $priceOrder = 0;
                foreach ($basket as $product) {
                    $priceOrder += $product->price * $product->cantidad;
                }
                $user = \Auth::user();
                $order = Order::create([
                'date'=>date('Y/m/d') ,
                'user_id'=>$user->id,
                'paid'=>$priceOrder]
                );

                foreach ($basket as $product) {
                    $order->products()->attach($product->id, ['quantity' => $product->cantidad, 'price' => $product->price]);
                }
                $request->session()->forget('basket');
            }
            return redirect('/basket');
    }

}

/*

 Crear la política:
    make:policy XxxxPolicy --model=Xxxx  //--model para indicar a que modelo va dirigida la politica
    Registrar la política en el AuthServiceProvider

    Usar la política (403):
    En el controlador:
    $this->authorize('regla', 'objeto') //ejemplo update/delete
    $this->authorize('regla', nombreClase) //ejemplo create

    En el controlador con "can"
    $user->can('regla', objeto/clase)

    En la vista con @can / @endcan
    @can('regla', objeto/clase)
    ...
    @endcan

Api

    Rutas en api.php
    Crear controlador: Api/EjemploController

Crear proyecto
composer create-project --prefer-dist laravel/laravel [nombre]
*/
