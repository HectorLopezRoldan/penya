<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cathegory;
use App\Product;
class CathegoryController extends Controller
{

function __construct()
{   //Si no estas logueado solo puedes ver index
    $this->middleware('auth')->except('index');
}

    public function index()
    {
        $cathegories = Cathegory::paginate(10);
        return view('cathegory.index', ['cathegories' => $cathegories]);
    }

    public function create()
    {
        return view('cathegory.create');
    }


    public function store(Request $request)
    {
        //validacion:
        $rules = [
            'name' => 'required|max:255|min:3',
        ];

        $request->validate($rules);

        $cathegory = new Cathegory();
        $cathegory->fill($request->all());
        $cathegory->save();

        return redirect('/cathegories');
    }

    public function destroy($id)
    {
        Cathegory::destroy($id);

        return back();
    }


   public function update(Request $request, $id)
    {

        $rules = [
            'name' => 'required|max:255|min:3',
        ];

        $request->validate($rules);

        $cathegory = Cathegory::findOrFail($id);
        $cathegory->fill($request->all());
        $cathegory->save();


        return redirect('/cathegories/' . $cathegory->id);
    }

    public function edit($id)
    {
        $cathegory = Cathegory::findOrFail($id);
        return view('cathegory.edit', ['cathegory' => $cathegory]);
    }


    public function show($id)
    {
        //$products = Product::with('cathegory')->findOrFail($id);
       // var_dump($products);
        $cathegory = Cathegory::findOrFail($id);
        $products = $cathegory->products;
        return view('cathegory.show', ['cathegory' => $cathegory,
            'products' => $products
            ]);
    }


}
