<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Cathegory;

class ProductController extends Controller
{

    function __construct()
    {   //Si no estas logueado solo puedes ver index
         $this->middleware('auth');
         //$this->middleware('guest');
    }

    public function index()
    {

        //$users = User::all();
        $products = Product::paginate(10);
        $cathegories = Product::paginate(10);
        // return $users;
        return view('product.index', [
            'products' => $products,
            'cathegories' => $cathegories]);
        //busca el fichero (uno de los dos):
        // /resources/views/user/index.php
        // /resources/views/user/index.blade.php
    }

    public function create()
    {
        return view('product.create');
    }

        public function store(Request $request)
    {
        //validacion:
        $rules = [
            'name' => 'required|max:255|min:3',
            //'price' => 'required|number',
            'cathegory_id' => 'required|max:255',
            // 'color' => 'required',
            // 'player' => 'required|max:255|min:5',
        ];

        $request->validate($rules);

        $product = new Product();
        $product->fill($request->all());
      //  $product->price = bcrypt($product->password);
        $product->save();

        return redirect('/products');
    }

        public function destroy($id)
    {
        Product::destroy($id);

        return back();
    }

    public function update(Request $request, $id)
    {

        $rules = [
            'name' => 'required|max:255|min:3',
            'price' => "required",
        ];

        $request->validate($rules);

        $product = Product::findOrFail($id);
        $product->fill($request->all());
        $product->save();


        return redirect('/products/' . $product->id);
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);
        return view('product.edit', ['product' => $product]);
    }


    public function show($id)
    {
        $cathegory = Product::with('cathegory')->findOrFail($id);

         $product = Product::findOrFail($id);
        return view('product.show', [
            'product' => $product,
            'cathegory' => $cathegory,
        ]);
    }

}
