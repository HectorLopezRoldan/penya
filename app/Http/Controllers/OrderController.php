<?php
//https://github.com/nahidulhasan/laravel-html2pdf
namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use NahidulHasan\Html2pdf\Facades\Pdf;
class OrderController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {

    $url=url()->current();
    $id=substr($url, -1);

        $user = \Auth::user();

        if($user->can('viewAllOrders', Order::class)){
            $orders = Order::paginate(10);
            return view('order.index',['orders' => $orders]);
        }elseif ($user->can('viewMyOrders', Order::class)){
            $orders = Order::where('user_id', 'like', $user->id)->paginate(10);
            return view('order.index',['orders' => $orders]);
        }else{
            return "err";
        }
    }


    public function toPDFshow()
    {

                //return $url;
    $url=url()->current();
    $id=substr($url, -1);
        $order = Order::findOrFail($id);
        $htmlPagado='';
        if($order->paid == 0){
            $htmlPagado='No';
        }else{
            $htmlPagado='Si';
        }


        $products= $order->products;
        $htmlHead='<h1>Este es el detalle del pedido <?php echo'. $order->id .'?></h1><ul><li>Fecha:'.date("d/m/Y", strtotime($order->date)).'.</li><li>Total:'.$order->total().' Euros</li><li>Pagado:'.$htmlPagado
            .'</li><li>Usuario:'.$order->user->name.'</li></ul>';

        $htmlBody='';
            foreach ($products as $product) {
  $htmlBody=$htmlBody.'<li>'.$product->name. '-' .$product->price.' Euros -'.$product->pivot->quantity.' unidades</li>';
            }


$htmlEnd='</ul>';


        $htmltoPDF =$htmlHead.'<h1>Productos del pedido</h1>'.$htmlBody.$htmlEnd;
        $document = Pdf::generatePdf($htmltoPDF);
        return Pdf::download();


    }


    public function toPDF()
    {
        $orders = Order::all();
        $htmlHead='<h1>Pedidos</h1><hr><table width="100%"><tr><th>Fecha</th><th>Precio total</th><th>Usuario</th><th>Pagado</th></tr>';
        $html='';
        foreach ($orders as $order) {
            $dateOrder=date("d/m/Y", strtotime($order->date));
            $totalOrder=$order->total();
            $userOrder=$order->user->name ;
            $pagadoOrder="";

            if($order->paid == 0){
                $pagadoOrder="No";
            }else{
                $pagadoOrder="Si";
            }

              $html=$html.'<tr><td>'.$dateOrder.'</td><td>'.$totalOrder.'</td><td>'.$userOrder.'</td><td>'.$pagadoOrder.'</td></tr>';
            }
        $htmlFin='</table>';



        $htmltoPDF =$htmlHead.$html.$htmlFin;
        $document = Pdf::generatePdf($htmltoPDF);
        return Pdf::download();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
       $products= $order->products;
        return view('order.show',['order' => $order,
        'products'=>$products]);
    }

    public function paid($id)
    {
        $order = Order::findOrFail($id);
        $order->paid = true;
        $order->save();
        return view('order.show',['order' => $order]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
