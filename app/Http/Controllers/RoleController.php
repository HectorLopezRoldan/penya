<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{

   public function index()
   {
       $this->authorize("index",Role::class);
       $roles = Role::paginate(10);
       return view('role.index', ['roles' => $roles]);

   }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //sin el with son N+1 accesos a bbdd (select)        //N usauarios + 1 role
        //con with un sólo acceso
        $role = Role::with('users')->findOrFail($id);
        $this->authorize('view',$role);
        return view('role.show', ['role' => $role]);
        // return $role;
    }

    public function edit($id)
    {
      //
    }


   public function update(Request $request, $id)
   {
       //
    }

   public function destroy($id)
   {
        $this->authorize('delete',$role);
   }
}
