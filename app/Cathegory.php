<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cathegory extends Model
{
        protected $fillable = [
        'name'
    ];

public function products()
{//Retorna todos los productos de la categoria que lo llama
   return $this->hasMany(Product::class);
}
}
