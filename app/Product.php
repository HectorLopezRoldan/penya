<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
       protected $fillable = [
        'name', 'price', 'cathegory_id'
    ];

    function cathegory() {
         return $this->belongsTo(Cathegory::class);
    }

}
