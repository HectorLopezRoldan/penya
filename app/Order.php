<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'date', 'user_id','paid'
    ];

    public function products()
    {
        //withPivot contiene las claves, quedaria añadir las otras 2 columnas
        return $this->belongsToMany(Product::class,'order_product')->withPivot('quantity','price');
    }

    public function total()
    {
        $total = 0;
        foreach ($this->products as $product) {
            $total += $product->pivot->quantity * $product->pivot->price;
        }
        return $total;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
