<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    public function boot()
{      
   Validator::extend(
          'recaptcha',
          'App\\Validators\\ReCaptcha@validate'
   );
}
}
